package ru.pixelsky.xmod.lang;

public class TimeFormat {
    public static String formatTime(long time) {
        int t = (int) (time % 20);
        time /= 20;

        int s = (int) (time % 60);
        time /= 60;

        int m = (int) (time % 60);
        time /= 60;

        int h = (int) (time % 24);
        time /= 24;

        int d = (int) (time);

        String f = (h > 0 || m > 0 || s > 0) ? String.format("%02d:%02d:%02d", h, m, s) : "";

        if (d > 0)
            return d + " суток " + f;
        else
            return f;
    }
}
