package ru.pixelsky.xmod.lang;

import net.minecraft.item.ItemStack;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to store localized names for items with metadata
 * Used in DefaultItemBlock
 */
public class NamesRegistry {
    private static Map<NameKey, String> registry = new HashMap<NameKey, String>();

    public static void put(int itemId, int damageValue, String name) {
        registry.put(nameKey(itemId, damageValue), name);
    }

    public static String get(ItemStack stack) {
        String name = registry.get(nameKey(stack));
        return name != null ? name : stack.getItem().itemID + ":" + stack.getItemDamage();
    }

    private static NameKey nameKey(ItemStack stack) {
        NameKey nameKey = new NameKey();
        nameKey.itemId = stack.getItem().itemID;
        nameKey.damageValue = stack.getItemDamage();
        return nameKey;
    }

    private static NameKey nameKey(int id, int dmg) {
        NameKey nameKey = new NameKey();
        nameKey.itemId = id;
        nameKey.damageValue = dmg;
        return nameKey;
    }

    private static class NameKey {
        private int itemId;
        private int damageValue;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            NameKey nameKey = (NameKey) o;
            if (damageValue != nameKey.damageValue) return false;
            if (itemId != nameKey.itemId) return false;
            return true;
        }

        @Override
        public int hashCode() {
            int result = itemId;
            result = 31 * result + damageValue;
            return result;
        }
    }
}
