package ru.pixelsky.xmod.network;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

public class XModPacketHandler implements IPacketHandler {
    private XModNotifyHandler notifyHandler = new XModNotifyHandler();
    private PingPacketHandler pingPacketHandler = new PingPacketHandler();

    @Override
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
        notifyHandler.onPacketData(manager, packet, player);
        pingPacketHandler.onPacketData(manager, packet, player);
    }
}
