package ru.pixelsky.xmod.network;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import net.minecraft.network.packet.Packet250CustomPayload;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PingUtils {
    private static long lastSendTime = 0;
    private static boolean pongReceived = true;
    private static int ping = 0;

    public static void updatePing() {
        try {
            Class clazz = Class.forName("ru.pixelsky.utils.PingManager");
            Method method = clazz.getMethod("setPing", int.class);
            method.invoke(null, getPing());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static int getPing() {
        if ((pongReceived && System.currentTimeMillis() - lastSendTime > 5000))
            sendPing();
        else if (!pongReceived && System.currentTimeMillis() - lastSendTime > 10000) {
            sendPing();
            return (int) (System.currentTimeMillis() - lastSendTime);
        }
        return ping;
    }

    public static void sendPing() {
        pongReceived = false;
        lastSendTime = System.currentTimeMillis();
        PacketDispatcher.sendPacketToServer(
                new Packet250CustomPayload("psky-ping", "ping".getBytes())
        );
    }

    public static void sendPong(Player player) {
        PacketDispatcher.sendPacketToPlayer(
                new Packet250CustomPayload("psky-ping", "pong".getBytes()), player
        );
    }

    public static void onPongReceived() {
        ping = (int) (System.currentTimeMillis() - lastSendTime);
        pongReceived = true;
    }
}
