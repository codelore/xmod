package ru.pixelsky.xmod.blocks.decoration;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import ru.pixelsky.xmod.blocks.BlockConstants;
import ru.pixelsky.xmod.blocks.BlockPartiallyTransparent;
import ru.pixelsky.xmod.core.XMod;

import java.util.List;
import java.util.Random;

/**
 * Stool block or half-block
 */
public class BlockColoredBrick extends BlockPartiallyTransparent {

    protected Icon[] icons = new Icon[16];

    public BlockColoredBrick(int id,Material mat) {
        super(id, mat);
        this.setCreativeTab(XMod.xModTab);
        this.setHardness(10.0f);

        setUnlocalizedName("colored-brick");
    }


    @SideOnly(Side.CLIENT)
    @Override
    public Icon getIcon(int side, int metadata) {
        return icons[metadata];
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister iconRegister) {
        for (int i = 0; i < icons.length; i++)
            icons[i] = iconRegister.registerIcon("xmod:build/brick/brick_" + i);
    }

    @Override
    public int quantityDropped(Random random) {
        return 1;
    }

    @Override
    public int idDropped(int par1, Random random, int par3) {
        return this.blockID;
    }

    @Override
    public int damageDropped(int metadata) {
        return metadata;
    }

    @Override
    public void getSubBlocks(int id, CreativeTabs tabs, List list) {
        for (int i = 0; i < 16; i++)
            list.add(new ItemStack(id, 1, i));
    }
}