/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.blocks.sakura;

import java.util.Random;
import ru.pixelsky.xmod.world.WorldGenSakura;
import net.minecraft.block.BlockFlower;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

/**
 * Sapling for sakura tree
 * Can't be got in survival mode
 */
public class BlockSakuraSapling extends BlockFlower
{
    public BlockSakuraSapling(int par1)
    {
        super(par1);
        float f = 0.4F;
        this.setCreativeTab(CreativeTabs.tabDecorations);
        setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 2.0F, 0.5F + f);
    }
    
    @Override
    public void registerIcons(IconRegister par1IconRegister) 
    {
        this.blockIcon = par1IconRegister.registerIcon("xmod:BlockSakuraSapling");
    }
    /**
     * Ticks the block if it's been scheduled
     */
    @Override
    public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        if (par1World.isRemote)
        {
            return;
        }

        super.updateTick(par1World, par2, par3, par4, par5Random);

        if (par1World.getBlockLightValue(par2, par3 + 1, par4) >= 9 && par5Random.nextInt(7) == 0)
        {
            int i = par1World.getBlockMetadata(par2, par3, par4);

            if ((i & 8) == 0)
            {
                par1World.setBlockMetadataWithNotify(par2, par3, par4, i | 8, 0);
            }
            else
            {
                growTree(par1World, par2, par3, par4, par5Random);
            }
        }
    }

    /**
     * Attempts to grow a sapling into a tree
     */

    public boolean func_50076_f(World par1World, int par2, int par3, int par4, int par5)
    {
        return par1World.getBlockId(par2, par3, par4) == blockID && (par1World.getBlockMetadata(par2, par3, par4) & 3) == par5;
    }

    /**
     * Determines the damage on the item the block drops. Used in cloth and wood.
     */
    
    @Override
    public int damageDropped(int par1)
    {
        return par1 & 3;
    }
    
    public void growTree(World world, int i, int j, int k, Random random)
    {
        int l = world.getBlockMetadata(i, j, k);
        world.setBlock(i, j, k, 0);
        Object obj = null;
        obj = new WorldGenSakura(true, l); //l - проброс метадаты
        if (!((WorldGenerator)(obj)).generate(world, random, i, j, k))
        {
            world.setBlockMetadataWithNotify(i, j, k, blockID, l);
        }
    }
}
