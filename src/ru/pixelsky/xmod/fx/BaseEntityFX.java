package ru.pixelsky.xmod.fx;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.World;

public class BaseEntityFX extends EntityFX {
    public static final String DEFAULT_TEXTURE = "/mods/xmod/textures/particles.png";

    protected String usedTexture;

    public BaseEntityFX(World par1World, double par2, double par4, double par6, double par8, double par10, double par12) {
        super(par1World, par2, par4, par6, par8, par10, par12);
        setUsedTexture(DEFAULT_TEXTURE);
    }

    public BaseEntityFX(World par1World, double par2, double par4, double par6) {
        super(par1World, par2, par4, par6);
        setUsedTexture(DEFAULT_TEXTURE);
    }

    @Override
    public void renderParticle(Tessellator par1Tessellator, float par2, float par3, float par4, float par5, float par6, float par7) {
        if (usedTexture != null)
            Minecraft.getMinecraft().renderEngine.bindTexture(usedTexture);
        super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6, par7);
    }

    protected void setUsedTexture(String usedTexture) {
        this.usedTexture = usedTexture;
    }
}
