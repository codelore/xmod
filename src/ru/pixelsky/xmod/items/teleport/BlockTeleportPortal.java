package ru.pixelsky.xmod.items.teleport;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import ru.pixelsky.xmod.core.XMod;
import ru.pixelsky.xmod.lang.TimeFormat;

import java.util.Random;

public class BlockTeleportPortal extends Block {

    public static final String LASTPORTAL_NBT = "lastportal";

    public BlockTeleportPortal(int id, Material mat) {
        super(id, mat);
        setLightValue(4);
        setCreativeTab(XMod.xModTab);
    }

    @Override
    public boolean hasTileEntity(int metadata) {
        return true;
    }

    @Override
    public Icon getIcon(int meta, int par2) {
        return Block.portal.getIcon(0, 0);
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return null;
    }

    public int quantityDropped(Random par1Random)
    {
        return 0;
    }

    public TileEntity createTileEntity(World world, int metadata)
    {
        return new TileEntityPortal();
    }

    public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
    {
        if (!(entity instanceof EntityPlayer))
            return;

        if (entity.getEntityData().hasKey(LASTPORTAL_NBT)) {
            /* Защита от циклических порталов */
            long val = entity.getEntityData().getLong(LASTPORTAL_NBT);
            if (System.currentTimeMillis() - val < 5000)
                return;
        }

        TileEntityPortal tile = getTileEntity(world, x, y, z);
        if (tile != null) {
            TeleportPlace place = tile.getPlace();
            if (place != null) {
                place.teleportPlayer((EntityPlayer) entity);
                entity.getEntityData().setLong(LASTPORTAL_NBT, System.currentTimeMillis());
            }
        }
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if (world.isRemote)
            return false;

        TileEntityPortal tileEntity = getTileEntity(world, x, y, z);
        player.addChatMessage("Осталось " + TimeFormat.formatTime(tileEntity.getActiveTime()));
        return false;
    }

    public TileEntityPortal getTileEntity(World world, int x, int y, int z) {
        return (TileEntityPortal) world.getBlockTileEntity(x, y, z);
    }
}
