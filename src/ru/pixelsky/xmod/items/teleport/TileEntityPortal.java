package ru.pixelsky.xmod.items.teleport;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityPortal extends TileEntity {
    private TeleportPlace place;
    private long activeTime = 20 * 10;

    @Override
    public boolean canUpdate() {
        return true;
    }

    @Override
    public void updateEntity() {
        if (worldObj.isRemote)
            return;

        activeTime--;
        if (activeTime <= 0) {
            worldObj.setBlockToAir(xCoord, yCoord, zCoord);
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        if (place != null)
            place.writeToNBT(compound);
        compound.setLong("aliveTime", activeTime);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        place = new TeleportPlace();
        place.readFromNBT(compound);
        compound.setLong("aliveTime", activeTime);
    }

    public void addActiveTime(long delta) {
        activeTime += delta;
    }

    public long getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(long activeTime) {
        this.activeTime = activeTime;
    }

    public TeleportPlace getPlace() {
        return place;
    }

    public void setPlace(TeleportPlace place) {
        this.place = place;
    }
}
