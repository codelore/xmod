package ru.pixelsky.xmod.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.util.StringTranslate;
import ru.pixelsky.xmod.core.XMod;
import ru.pixelsky.xmod.lang.NamesRegistry;

public class DefaultBlockItem extends ItemBlock {
    public DefaultBlockItem(int id) {
        super(id);
        this.setHasSubtypes(true);
    }

    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public Icon getIconFromDamage(int damage) {
        return Block.blocksList[getBlockID()].getIcon(1, damage);
    }

    @Override
    public String getLocalizedName(ItemStack itemStack) {
        return NamesRegistry.get(itemStack);
    }

    public String getItemDisplayName(ItemStack itemStack)
    {
        return getLocalizedName(itemStack);
    }
}
