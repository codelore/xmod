package ru.pixelsky.xmod.core;

import cpw.mods.fml.common.IFuelHandler;
import net.minecraft.item.ItemStack;

public class XModFuelHandler implements IFuelHandler {
    @Override
    public int getBurnTime(ItemStack fuel) {
        if (fuel.getItem().itemID == XMod.blockCoal.blockID) {
            return 16000;
        }
        return 0;
    }
}
