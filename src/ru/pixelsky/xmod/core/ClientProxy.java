/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.core;

import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import ru.pixelsky.xmod.tick.ScreenMessageHandler;
import ru.pixelsky.xmod.tick.XNotify;

/**
 *
 * @author Smile
 */
public class ClientProxy extends CommonProxy
{
    public static final XNotify xNotify = new XNotify();
    
    @Override
    public void registerNotifyRender()
    {
        TickRegistry.registerTickHandler(xNotify, Side.CLIENT);
    }

    @Override
    public void addLowMessageToArray(String s) {
        ScreenMessageHandler.addLowMessage(s, 500);
    }

    @Override
    public void addHightMessageToArray(String s) {
        ScreenMessageHandler.addHightMessage(s, 10000, 0xa50b00);
    }
}
