/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.xmod.tick;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.ScaledResolution;
import ru.pixelsky.xmod.network.PingUtils;

import java.util.EnumSet;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author Smile
 */
public class XNotify implements ITickHandler
{
    private Minecraft mc;
    
    public XNotify()
    {
        mc = Minecraft.getMinecraft();
    }
 
    public static String stripTags(String var0)
    {
        return Pattern.compile("[\\\u00a7][0-9a-fklmnor]").matcher(var0).replaceAll("");
    }

    public static int stringWidth(Minecraft var0, String var1)
    {
        return var0.fontRenderer.getStringWidth(stripTags(var1));
    }
    
    @Override
    public void tickEnd(EnumSet<TickType> type, Object... tickData)
    {
        ScaledResolution scaledRes = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);

        ScreenMessageHandler sm;
                
        List<ScreenMessageHandler> msgList = ScreenMessageHandler.getMsgList();

        int sWidth;
        int sHeight;
        
        for(int i = 0; i < msgList.size(); i++)
        {
            sm = msgList.get(i);
            sm.onUpdate();
   
            if(!sm.shouldShow())
            {
                msgList.remove(i);
                continue;
            }

            sWidth = (scaledRes.getScaledWidth() / 2) - (stringWidth(mc, sm.getMessage()) / 2);
            sHeight = 15 + i * 10;
            this.renderOnScreen(sWidth, sHeight, sm);
        }

        PingUtils.updatePing();
    }
    
    @Override
    public void tickStart(EnumSet<TickType> type, Object... tickData)
    {
        
    }
    
    @Override
    public String getLabel()
    {
        return "xNotify";
    }
    
    @Override
    public EnumSet<TickType> ticks()
    {
        return EnumSet.of(TickType.RENDER);
    }
    
    public void renderOnScreen(int width, int height, ScreenMessageHandler msg)
    {    
        if ((mc.thePlayer != null && mc.currentScreen == null) ||(mc.thePlayer != null && mc.currentScreen == new GuiChat())) 
        {
            
            if(msg.getMessage().isEmpty())
            {
                return;
            }
            
            mc.entityRenderer.setupOverlayRendering();
            this.drawString(width, height, msg.getColor() | (int)(msg.getAlpha() * 255) << 24, msg.getMessage(), "!");
        }
    } 
      
    public void drawString(int x, int y, int colour, String formatString, Object...args)
    {
        FontRenderer fr = mc.fontRenderer;
        String s = String.format(formatString, args);
        fr.drawStringWithShadow(s, x, y, colour);
    }
}